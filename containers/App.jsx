import React, {Component, PropTypes} from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Field from '../components/Field';
import Control from '../components/Control';

class App extends Component {
    render() {
        return (
            <div>
                <Field />
                <Control />
            </div>
        );
    }
}

export default App;
