import * as type from '../constants/ActionTypes';
import * as Players from '../constants/Players';

const initialState = {
    field: [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ],
    wonPlayer: Players.EMPTY,
    currentPlayerTurn: Players.PLAYER_1
};

/**
 * Checking who won, returns player id or empty if there is no winner
 *
 * @param array field
 */
let checkWinPlayer = (field) => {
    let lines = [];
    lines[Players.PLAYER_1] = [0,0,0, 0,0,0, 0,0]; //3 horozontal, 3 vertical, 2 diagonal
    lines[Players.PLAYER_2] = [0,0,0, 0,0,0, 0,0];
    for (let x = 0; x < 3; x++) {
        for (let y = 0; y < 3; y++) {
            if (field[x][y] !== Players.EMPTY) {
                lines[field[x][y]][x]++; //adding to horizonal line
                lines[field[x][y]][y + 3]++; //adding to vertical line
                if (x === y) {
                    lines[field[x][y]][6]++; //adding to diagonal line
                }
                if (x === 2 - y) {
                    lines[field[x][y]][7]++; //adding to r-diagonal line
                }
            }
        }
    }

    for (let i = 0; i < 8; i++) {
        if (lines[Players.PLAYER_1][i] === 3) {
            return Players.PLAYER_1;
        }
        if (lines[Players.PLAYER_2][i] === 3) {
            return Players.PLAYER_2;
        }
    }

    return Players.EMPTY;
}

/**
 * Makes turn and checks for end conditions
 *
 * @param object state
 * @param object action
 * @returns object
 */
let makeTurn = (state, action) => {
    let newField = [];
    let currentPlayerTurn = state.currentPlayerTurn;
    let emptyCells = false;
    for (let x = 0; x < 3; x++) {
        let newFieldRow = [];
        for (let y = 0; y < 3; y++) {
            if (action.x === x && action.y === y) {
                newFieldRow.push(state.currentPlayerTurn);
                if (state.currentPlayerTurn === Players.PLAYER_1) {
                    currentPlayerTurn = Players.PLAYER_2;
                } else {
                    currentPlayerTurn = Players.PLAYER_1;
                }
            } else {
                newFieldRow.push(state.field[x][y]);
                if (state.field[x][y] === Players.EMPTY) {
                    emptyCells = true;
                }
            }

        }
        newField.push(newFieldRow);
    }
    if (!emptyCells) {
        currentPlayerTurn = Players.EMPTY;
    }
    return Object.assign({}, state, {
        field: newField,
        wonPlayer: checkWinPlayer(newField),
        currentPlayerTurn: currentPlayerTurn
    });
}

export default function game(state = initialState, action) {
    switch (action.type) {
        case type.TAKE_CELL:
            if (state.field[action.x][action.y] === Players.EMPTY && state.wonPlayer === Players.EMPTY) {
                return makeTurn(state, action);
            }
            return state;
        case type.NEW_GAME:
            return initialState;
        default:
            return state;
    }
};

