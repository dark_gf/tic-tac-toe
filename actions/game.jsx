import * as types from '../constants/ActionTypes';

export function newGame() {
    return { type: types.NEW_GAME};
}
