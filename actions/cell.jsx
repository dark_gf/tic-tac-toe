import * as types from '../constants/ActionTypes';

export function takeCell(x, y) {
    return { type: types.TAKE_CELL, x, y};
}
