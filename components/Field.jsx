import React, { Component, PropTypes } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Cell from '../components/Cell';

class Field extends Component {
    render() {
        const { field } = this.props;
        let gameField = [];
        for (let x = 0; x < 3; x++) {
            let gameFieldRow = [];
            for (let y = 0; y < 3; y++) {
                gameFieldRow.push(<Cell x={x} y={y} value={field[x][y]} />);
            }
            gameField.push(gameFieldRow);
        }
        return (
            <div className="field">
                {gameField}
            </div>
        );
    }
}


Field.propTypes = {
    field: PropTypes.array.isRequired
};

function mapStateToProps(state) {
    return {
        field: state.game.field
    };
}

function mapDispatchToProps(dispatch) {
    return {
        //actions: bindActionCreators(TodoActions, dispatch)
    };
}

export default connect(
    mapStateToProps
)(Field);