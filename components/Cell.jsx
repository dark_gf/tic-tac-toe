import React, { Component, PropTypes } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as CellActions from '../actions/cell';
import * as Players from '../constants/Players'

class Cell extends Component {
    render() {
        const { x, y, value, actions } = this.props;
        let className = "cell cell-" + x + "-" + y;
        let cellValue = "";
        if (value === Players.PLAYER_1) {
            cellValue = 'x';
        } else if (value === Players.PLAYER_2) {
            cellValue = 'o';
        }
        return (
            <div className={className} onClick={() => actions.takeCell(x, y)}>
                {cellValue}
            </div>
        );
    }
};

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(CellActions, dispatch)
    };
}

export default connect(null, mapDispatchToProps)(Cell);
