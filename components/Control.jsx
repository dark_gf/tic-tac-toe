import React, { Component, PropTypes } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { RaisedButton } from 'material-ui';
import * as GameActions from '../actions/game';
import * as Players from '../constants/Players';

class Control extends Component {
    render() {
        const { wonPlayer, currentPlayerTurn, actions } = this.props;
        let message;
        if (wonPlayer === Players.PLAYER_1) {
            message = "Congratulations! X player WON!";
        } else if (wonPlayer === Players.PLAYER_2) {
            message = "Congratulations! O player WON!";
        } else if (currentPlayerTurn === Players.PLAYER_1) {
            message = "It's a X player turn.";
        } else if (currentPlayerTurn === Players.PLAYER_2) {
            message = "It's a O player turn.";
        } else {
            message = "It's a draw.";
        }
        return (
            <div>
                <div className="status">{message}</div>
                <RaisedButton label="New Game" primary={true} onClick={() => actions.newGame()} />
            </div>
        );
    }
};

function mapStateToProps(state) {
    return {
        wonPlayer: state.game.wonPlayer,
        currentPlayerTurn: state.game.currentPlayerTurn
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(GameActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Control);
